Name:           tetzle
Version:        2.2.0
Release:        1%{?dist}
Summary:        Tetromino jigsaw puzzle game
License:        GPLv3+
URL:            http://gottcode.org/%{name}/
Source0:        http://gottcode.org/%{name}/%{name}-%{version}-src.tar.bz2

BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  qt5-qttools-devel

Requires:       hicolor-icon-theme

%description
Tetzle is a jigsaw puzzle game that uses tetrominoes for the pieces. Any image
can be imported and used to create puzzles with a wide range of sizes. Games
are saved automatically, and you can select between currently in progress games.

%prep
%setup -q

%build
%{qmake_qt5} PREFIX=%{_prefix}
make %{?_smp_mflags}

%install
make install INSTALL_ROOT=%{buildroot}

%find_lang %{name} --with-qt --without-mo

%check
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop || :
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/%{name}.appdata.xml || :

%files -f %{name}.lang
%doc ChangeLog CREDITS README
%license COPYING
%{_bindir}/%{name}
%{_datadir}/metainfo/%{name}.appdata.xml
%{_datadir}/applications/%{name}.desktop
%dir %{_datadir}/%{name}/
%dir %{_datadir}/%{name}/translations
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_mandir}/man6/%{name}.6.*


%changelog
* Mon Nov 29 2021 Yaakov Selkowitz <yselkowi@redhat.com> - 2.2.0-1
- new version

* Fri Jul 31 2020 Yaakov Selkowitz <yselkowi@redhat.com> - 2.1.6-1
- Initial package
